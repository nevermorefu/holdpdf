import tempfile

from django.shortcuts import render, get_object_or_404

from weasyprint import HTML, CSS
from django.template.loader import get_template, render_to_string
from django.http import HttpResponse

# Create your views here.
from pdf_app.models import Case


def cases_view(request):
    """Display all blog posts"""

    cases = Case.objects.all()

    return render(request, 'index.html', {'cases': cases})


def case_view(request, pk):
    """Display specific blog posts"""

    case = get_object_or_404(Case, pk=pk)

    return render(request, 'case.html', {'case': case})


def case_download(request, pk):
    """Generate pdf."""
    # Model data
    case = Case.objects.filter(pk=pk)

    # Rendered
    html_string = render_to_string('case_download.html', {'case': case})
    html = HTML(string=html_string)
    result = html.write_pdf()

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'r')
        response.write(output.read())

    return response
