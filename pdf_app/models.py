import io
import os

from django.db import models
from martor.models import MartorField
# from reportlab.pdfgen import canvas
# from reportlab.lib.pagesizes import letter

from pdf.settings import MEDIA_ROOT

# Create your models here.


class Case(models.Model):
    title = models.CharField(max_length=1000)
    summary = MartorField()
    TYPE_CHOICE = {
        ('type1', 'type1'),
        ('type2', 'type2')
    }
    type = models.CharField(choices=TYPE_CHOICE, max_length=100)
    file1 = models.FileField(upload_to='upload', null=True, blank=True)
    file2 = models.FileField(upload_to='upload', null=True, blank=True)
    report = models.FileField(null=True, blank=True)

    def __str__(self):
        return self.title

    # def save(self, *args, **kwargs):
    #     file = os.path.join(MEDIA_ROOT, self.title + '_pdf.pdf')
    #
    #     # Create the PDF object, using the buffer as its "file."
    #     p = canvas.Canvas(file, pagesize=letter)
    #
    #     # Draw things on the PDF. Here's where the PDF generation happens.
    #     # See the ReportLab documentation for the full list of functionality.
    #     p.setLineWidth(.3)
    #     p.setFont('Helvetica', 12)
    #
    #     p.drawString(30, 750, self.title)
    #     p.drawString(30, 735, self.summary)
    #     p.drawString(500, 750, "12/12/2010")
    #     p.line(480, 747, 580, 747)
    #
    #     p.drawString(275, 725, 'AMOUNT OWED:')
    #     p.drawString(500, 725, "$1,000.00")
    #     p.line(378, 723, 580, 723)
    #
    #     p.drawString(30, 703, 'RECEIVED BY:')
    #     p.line(120, 700, 580, 700)
    #     p.drawString(120, 703, "JOHN DOE")
    #
    #     # Close the PDF object cleanly, and we're done.
    #     p.showPage()
    #     p.save()
    #
    #     self.report = file
    #
    #     super(Case, self).save(*args, **kwargs)


class This(models.Model):
    this = models.CharField(max_length=1000)
    this1 = models.CharField(max_length=1000)
    this2 = models.CharField(max_length=1000)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)

    def __str__(self):
        return self.this + '-' + \
               self.this1 + '-' + \
               self.this2


class That(models.Model):
    that = models.CharField(max_length=1000)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
