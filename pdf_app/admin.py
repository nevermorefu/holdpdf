from django.contrib import admin
from advanced_filters.admin import AdminAdvancedFiltersMixin
from pdf_app.models import Case, That, This

# Register your models here.


class ThisAdmin(admin.TabularInline):
    model = This


class ThatAdmin(admin.TabularInline):
    model = That


@admin.register(Case)
class CaseAdmin(AdminAdvancedFiltersMixin, admin.ModelAdmin):
    exclude = ('report',)
    inlines = [ThatAdmin, ThisAdmin, ]

    list_filter = ('type', )   # simple list filters

    # specify which fields can be selected in the advanced filter
    # creation form
    advanced_filter_fields = (
        'title',
        'summary',
        'type',

        # even use related fields as lookup fields
        'case__this',
        'case__that',
    )


@admin.register(This)
class CaseAdmin(AdminAdvancedFiltersMixin, admin.ModelAdmin):
    exclude = ()
    # specify which fields can be selected in the advanced filter
    # creation form
    advanced_filter_fields = (
        'this',
        'this1',
        'this2',
    )


@admin.register(That)
class CaseAdmin(AdminAdvancedFiltersMixin, admin.ModelAdmin):
    exclude = ()
    # specify which fields can be selected in the advanced filter
    # creation form
    advanced_filter_fields = (
        'that',
    )
